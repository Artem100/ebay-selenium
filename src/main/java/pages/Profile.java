package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class Profile {

    public WebDriver driver;

    public  Profile(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle title = ResourceBundle.getBundle("titles");

    @FindBy(id = "edit")
    private WebElement buttonEdit;

    @FindBy(css = "a.icons > form > input[name='imgbin']")
    private WebElement buttonUpdloadAvater;

    @FindBy(css = "button#done")
    private WebElement buttonDoneEditing;

    @FindBy(css = "a.rmv")
    private WebElement buttonRemovePhoto;

    @FindBy(css = "div.icon > div.acts > a.upld")
    private WebElement buttonChangePhoto;

    @FindBy(css = "a.icons.edit_icn.noUpld.pen")
    private WebElement buttonEditPhoto;

    public void assertProfile(){
        Assert.assertTrue(driver.getTitle().contains(title.getString("profileOfMa_9843")));
    }


    public void uploadNewAvatar(){
        buttonEdit.click();
        try {buttonEditPhoto.click();
            buttonChangePhoto.click();
            buttonChangePhoto.sendKeys(System.getProperty("user.dir") + "/testFiles/dog.jpeg");
        }catch (Exception e){buttonUpdloadAvater.click();
            buttonUpdloadAvater.sendKeys(System.getProperty("user.dir") + "/testFiles/cat.jpg");
        }
        buttonDoneEditing.click();
    }
}
