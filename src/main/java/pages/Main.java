package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class Main {

    public WebDriver driver;

    public Main(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle titles = ResourceBundle.getBundle("titles");

    @FindBy(css = ".gh-ug-guest > a")
    private WebElement buttonSignIn;

    @FindBy(id = "gh-ac")
    private WebElement fieldSearch;

    @FindBy(id = "gh-btn")
    private WebElement buttonSearch;

    @FindBy(css ="button#gh-ug.gh-ua.gh-control")
    private WebElement buttonProfile;

    @FindBy(css = "li#gh-up")
    private WebElement buttonAvatar;


    public void assertMainPage(){
        Assert.assertTrue(driver.getTitle().contains(titles.getString("MainPage")));
    }

    public void gotoSingInPage(){
        buttonSignIn.click();
    }

    public void searchGoods(String word){
        assertMainPage();
        fieldSearch.sendKeys(word);
        buttonSearch.click();
    }

    public void gotoAvatarPage(){
        assertMainPage();
        Actions action = new Actions(driver);
        action.moveToElement(buttonProfile).build().perform();
        buttonAvatar.click();
    }




}
