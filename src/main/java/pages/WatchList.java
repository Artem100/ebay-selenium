package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class WatchList {

    public WebDriver driver;

    public WatchList(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "div.item-title.hasvariations>a")
    private WebElement titleOfGoods1;

    @FindBy(css = "div.item-title.hasvariations")
    private WebElement titleOfGoods2;

    @FindBy(css = ".action-clmn.item-clmn >.m-checkbox")
    private WebElement checkBoxOfGoods;

    @FindBy(css = "button.default.small.secondary")
    private WebElement buttonDelete;

    private static ResourceBundle title = ResourceBundle.getBundle("titles");
    private static ResourceBundle rb2 = ResourceBundle.getBundle("testData");


    public void assertMainPage(){
        Assert.assertTrue(driver.getTitle().contains(title.getString("watchList")));
    }

    public void watchList(){
        assertMainPage();
        Assert.assertTrue(titleOfGoods1.getText().contains(rb2.getString("watchListSamsung")));
        //String title = driver.findElement(By.cssSelector("div.item-title.hasvariations>a")).getText();
        checkBoxOfGoods.click();
        buttonDelete.click();
    }



}
