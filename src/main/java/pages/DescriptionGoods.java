package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DescriptionGoods {

    public WebDriver driver;

    public DescriptionGoods(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "a#watchLink")
    private WebElement buttonAddtoWatchList;

    @FindBy(id = "w1-6-_lmsg")
    private WebElement messageAddList;

    @FindBy(css = "div.msgPad")
    private WebElement iconWatchList;

    @FindBy(css = "#w1-6-_lmsg > a")
    private WebElement gotoWatchList;

    @FindBy(name = "Color")
    private WebElement selectColor;

    @FindBy(css = "a#isCartBtn_btn")
    private WebElement buttonAddToCard;


    public void addToWatchList() throws InterruptedException{
        try {Select dropdown= new Select(selectColor);
            dropdown.selectByIndex(1);
            if (buttonAddtoWatchList.isDisplayed()){buttonAddtoWatchList.click(); }
            else; }
        catch (Exception e){}
        iconWatchList.isDisplayed();
        messageAddList.getText().contains("Added to your Watch list");
        Thread.sleep(3000);
        gotoWatchList.click();
    }

    public void addToCard(){
        try {Select dropdown= new Select(selectColor);
            dropdown.selectByIndex(1);}
        catch (Exception e){}
        buttonAddToCard.click();
    }




}
