package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ResourceBundle;

public class SignIn {
    public WebDriver driver;

    public SignIn(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(id = "userid")
    private WebElement fieldUsername;

    @FindBy(id = "pass")
    private WebElement fieldPassword;

    @FindBy(id = "sgnBt")
    private WebElement buttonSubmit;

    private static ResourceBundle rb = ResourceBundle.getBundle("titles");

    public void assertSingInPage(){
        Assert.assertTrue(driver.getTitle().contains(rb.getString("SignIn")));
    }


    public void SignIn(String username, String password){
        assertSingInPage();
        fieldUsername.sendKeys(username);
        fieldPassword.sendKeys(password);
        buttonSubmit.click();
    }


}
