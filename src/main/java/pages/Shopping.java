package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;
import java.util.ResourceBundle;

public class Shopping {

    public WebDriver driver;
    String totalCount1="174.99";

    public Shopping(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    private static ResourceBundle title = ResourceBundle.getBundle("titles");
    private static ResourceBundle pages = ResourceBundle.getBundle("pages");

    @FindBy(css = "div.cart-bucket-lineitem")
    private List<WebElement> selectorGoods;

    @FindBy(css = "td.val-col.total-row > span > span")
    private WebElement fieldTotalCount;

    @FindBy(css = "[data-test-info='{\"sellerId\":\"ozdiscounts\"}']")
    private WebElement locatorSamsungGalaxyS6;

    @FindBy(css = "[data-test-info='{\"sellerId\":\"mamount123\"}']")
    private WebElement locatorGooglePixel32GB;

    @FindBy(css = "td.val-col.total-row")
    private WebElement fieldTotalcount;

    @FindBy(css = "[data-test-id='cart-remove-item']")
    private List<WebElement> buttonRemove;

    public void assertShopping(){
        Assert.assertTrue(driver.getTitle().contains(title.getString("shopping"))); }


    public void checkSamsungInShopping(){
        assertShopping();
        Assert.assertTrue(selectorGoods.size()==1);
        Assert.assertTrue(locatorSamsungGalaxyS6.isDisplayed());
    }

    public void checkGooglePixel32GB(){
        assertShopping();
        Assert.assertTrue(locatorGooglePixel32GB.isDisplayed());
    }

    public void checkDisplayTwoGoods()throws InterruptedException{
        assertShopping();
        Assert.assertTrue(selectorGoods.size()==2);
        Assert.assertTrue(locatorSamsungGalaxyS6.isDisplayed());
        Assert.assertTrue(locatorGooglePixel32GB.isDisplayed());
        clickRemovebutton();
    }

    public void clickRemovebutton() throws InterruptedException{
        for (WebElement check : buttonRemove){
            check.click();
            Thread.sleep(1000); // sleep is necessary because page is refreshed after remove of one goods
        }
    }

    public void removeOldGoods() throws InterruptedException{
        driver.get(pages.getString("CartPage"));
        assertShopping();
        clickRemovebutton();
            System.out.println("Cart was empty");
        }
    }


