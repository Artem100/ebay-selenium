package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.ResourceBundle;

public class Search {

    public WebDriver driver;

    public Search(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "input[type='checkbox'][aria-label='Samsung Galaxy S6']")
    private WebElement checkboxSamsungGalaxyS6;

    @FindBy(css = "input[type='checkbox'][aria-label='64 GB']")
    private WebElement checkbox64GB;

    @FindBy(css = "input[type='checkbox'][aria-label='3G']")
    private WebElement checkbox3G;

    @FindBy(css = "input[type='checkbox'][aria-label='Manufacturer refurbished']")
    private WebElement checkboxManufacturer;

    @FindBy(css = "input[aria-label='Maximum Value']")
    private WebElement fieldMaximumValue;

    @FindBy(css = "input.price[name='_udhi']")
    private WebElement fieldMaximumValue2;

    @FindBy(xpath = "//div[contains(text(),'64 GB')]/span[contains(@class, 'clipped')]")
    private WebElement filter64GBDisplayed;

    @FindBy(xpath = "//div[contains(text(),'New')]/span[contains(@class, 'clipped')]")
    private WebElement filterNewDisplayed;

    @FindBy(xpath = "//div[contains(text(),'3G')]/span[contains(@class, 'clipped')]")
    private WebElement filter3GDisplayed;

    @FindBy(xpath = "//div[contains(text(),'Samsung Galaxy S6')]/span[contains(@class, 'clipped')]")
    private WebElement filterSamsungS6Displayed;

    @FindBy(css = "li.s-item")
    private List<WebElement> samsungS6Result1;

    @FindBy(css = "li.sresult")
    private List<WebElement> samsungS6Result2;

    @FindBy(css = "div.s-item__image-wrapper > .s-item__image-img")
    private WebElement clickSamsung1;

    @FindBy(css = ".lvpicinner > .img.imgWr2")
    private WebElement clickSamsung2;


    private static ResourceBundle rb = ResourceBundle.getBundle("testData");


    public void searchCanon(){
        String wordForSearch = rb.getString("canonSearch");
        String searchResult = "//li[contains(@id, 'srp-river-results-listing1')]//h3[contains(text(),'"+wordForSearch+"')]";
        String searchResult2 = "li[r='1'] > h3.lvtitle > a";
        System.out.println(searchResult);
        System.out.println(searchResult2);
        Assert.assertTrue(driver.findElement(By.xpath(searchResult)).isDisplayed()
                ||driver.findElement(By.cssSelector(searchResult2)).getText().contains(wordForSearch));
    }

    public void fieldMaxiumPrice(String price){
        try{
            fieldMaximumValue.isDisplayed();
            fieldMaximumValue.sendKeys(price);
        fieldMaximumValue.sendKeys(Keys.ENTER);}
        catch (Exception e){
        fieldMaximumValue2.sendKeys(price);
        fieldMaximumValue2.sendKeys(Keys.ENTER);}
    }


    public void filterforSamsung(){
        fieldMaxiumPrice("220");
        checkboxSamsungGalaxyS6.click();
        checkbox64GB.click();
        checkbox3G.click();
        checkboxManufacturer.click();
        Assert.assertTrue(samsungS6Result1.size()==1||samsungS6Result2.size()==1);
    }

    public void clickSamsungS6(){
        try { clickSamsung1.click(); }
        catch (Exception e){ clickSamsung2.click(); }
    }

    public void clickSamsungS61() {
        try {
            clickSamsung2.click();
        } catch (Exception e) {
        }

    }





}
