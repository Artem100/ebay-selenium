import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;
import ru.yandex.qatools.allure.annotations.Description;
import setup.UiSetup;


import java.util.ResourceBundle;

public class UI {

    public static Main main;
    public static SignIn signIn;
    public static Search search;
    public static DescriptionGoods descriptionGoods;
    public static WatchList watchList;
    public static Shopping shopping;
    public static Profile profile;
    private static ResourceBundle credentials = ResourceBundle.getBundle("credentials");
    private static ResourceBundle testData = ResourceBundle.getBundle("testData");
    private static ResourceBundle pages = ResourceBundle.getBundle("pages");

    @BeforeMethod
    public void setup(){
        UiSetup.prepareUI();
        main = new Main(UiSetup.driver);
        signIn = new SignIn(UiSetup.driver);
        search = new Search(UiSetup.driver);
        descriptionGoods = new DescriptionGoods(UiSetup.driver);
        watchList = new WatchList(UiSetup.driver);
        shopping = new Shopping(UiSetup.driver);
        profile = new Profile(UiSetup.driver);
    }

    @Test(priority = 0)
    @Description("Check the positive login")
    public void login(){
        main.gotoSingInPage();
        signIn.SignIn((credentials.getString("username")),(credentials.getString("password")));
        main.assertMainPage();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Test the searching of goods by his full name")
    public void test01(){
        login();
        main.searchGoods((testData.getString("canon")));
        search.searchCanon();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of filters for searching of goods")
    public void test02(){
        login();
        main.searchGoods("Samsung");
        search.filterforSamsung();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of function WatchList of goods")
    public void test03() throws InterruptedException{
        login();
        UiSetup.openURL((pages.getString("SamsungGalaxyS6")));
        descriptionGoods.addToWatchList();
        watchList.watchList();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of total price for two goods")
    public void test04() throws InterruptedException{
        login();
        shopping.removeOldGoods();
        UiSetup.openURL((pages.getString("SamsungGalaxyS6")));
        descriptionGoods.addToCard();
        shopping.checkSamsungInShopping();
        UiSetup.openURL((pages.getString("GooglePixel")));
        descriptionGoods.addToCard();
        shopping.checkDisplayTwoGoods();
    }

    @Test(dependsOnMethods = {"login"})
    @Description("Testing of function for changing of avatars")
    public void test05(){
        login();
        main.gotoAvatarPage();
        profile.uploadNewAvatar();
    }

    @AfterMethod
    public void close(){
        UiSetup.driver.close();
    }

}
